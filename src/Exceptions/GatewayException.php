<?php

namespace eValor\BiliOpenLiveSdk\Exceptions;

/**
 * 网关异常
 * Class GatewayException
 * @package eValor\BiliOpenLiveSdk\Exceptions
 */
class GatewayException extends Exception
{

}
