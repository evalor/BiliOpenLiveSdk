<?php

namespace eValor\BiliOpenLiveSdk\Exceptions;

/**
 * 基础异常
 * Class Exception
 * @package eValor\BiliOpenLiveSdk\Exceptions
 */
class Exception extends \Exception
{

}
