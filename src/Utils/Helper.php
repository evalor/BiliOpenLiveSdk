<?php

namespace eValor\BiliOpenLiveSdk\Utils;

use Exception;

/**
 * 助手支持类
 * Class Helper
 * @package Yeebok\DtTripSdk\Utils
 */
class Helper
{
    /**
     * 生成随机字母数字字符串
     * @param int $length
     * @param string $alphabet
     * @return false|string
     */
    public static function randomCharacter(int $length = 6, string $alphabet = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789')
    {
        mt_srand();  // 重新随机种子
        // 重复字母表以防止生成长度溢出字母表长度
        if ($length >= strlen($alphabet)) {
            $rate = intval($length / strlen($alphabet)) + 1;
            $alphabet = str_repeat($alphabet, $rate);
        }
        // 打乱顺序返回
        return substr(str_shuffle($alphabet), 0, $length);
    }

    /**
     * 返回毫秒时间戳
     * @return int
     */
    public static function microTimestamp(): int
    {
        return intval(microtime(true) * 1000);
    }

    /**
     * 进行Json编码
     * @param mixed $value 需要编码的内容
     * @param int $options 编码选项
     * @param int $depth 最大编码深度
     * @return false|string
     * @throws Exception
     */
    public static function jsonEncode($value, int $options = JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES, int $depth = 512)
    {
        $json = json_encode($value, $options, $depth);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new Exception(
                'json_encode error: ' . json_last_error_msg()
            );
        }

        return $json;
    }

    /**
     * 进行Json解码
     * @param string $json 需要解码的内容
     * @param bool $assoc 是否解码为关联数组
     * @param int $depth 解码最大深度
     * @param int $options 解码选项
     * @return mixed
     * @throws Exception
     */
    public static function jsonDecode(string $json, bool $assoc = true, int $depth = 512, int $options = 0)
    {
        $data = json_decode($json, $assoc, $depth, $options);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new Exception(
                'json_decode error: ' . json_last_error_msg()
            );
        }

        return $data;
    }
}
