<?php

namespace eValor\BiliOpenLiveSdk\Service;

use eValor\BiliOpenLiveSdk\BasicService;
use eValor\BiliOpenLiveSdk\Exceptions\GatewayException;
use Throwable;

/**
 * 直播相关接口
 * Class Live
 * @package eValor\BiliOpenLiveSdk\Service
 */
class Live extends BasicService
{
    /**
     * 使用主播身份码启动新游戏
     * @param $idCode
     * @return mixed
     * @throws GatewayException
     * @throws Throwable
     */
    public function appStart($idCode)
    {
        return $this->httpPostWithSign(
            '/v2/app/start',
            [
                'code'   => $idCode,
                'app_id' => $this->config['appId']
            ]
        );
    }

    /**
     * 使用游戏ID结束本场游戏
     * @param $gameId
     * @return mixed
     * @throws GatewayException
     * @throws Throwable
     */
    public function appEnd($gameId)
    {
        return $this->httpPostWithSign(
            '/v2/app/end',
            [
                'game_id' => $gameId,
                'app_id'  => $this->config['appId']
            ]
        );
    }

    /**
     * 发送场次心跳
     * @param $gameId
     * @return mixed
     * @throws GatewayException
     * @throws Throwable
     */
    public function appHeartbeat($gameId)
    {
        return $this->httpPostWithSign(
            '/v2/app/heartbeat',
            [
                'game_id' => $gameId
            ]
        );
    }

    /**
     * 批量发送场次心跳
     * @param array $gameIds
     * @return mixed
     * @throws GatewayException
     * @throws Throwable
     */
    public function appBatchHeartbeat(array $gameIds = [])
    {
        return $this->httpPostWithSign(
            '/v2/app/batchHeartbeat',
            [
                'game_ids' => $gameIds
            ]
        );
    }
}
