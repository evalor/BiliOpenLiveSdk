<?php

namespace eValor\BiliOpenLiveSdk;

use eValor\BiliOpenLiveSdk\Exceptions\Exception;
use eValor\BiliOpenLiveSdk\Service\Live;

/**
 * 哔哩哔哩直播开放平台Sdk
 * Class BiLiveSdk
 * @package eValor\BiliOpenLiveSdk
 * @property Live $live 直播服务
 */
class BiLiveSdk
{
    /**
     * 当前Sdk版本号
     * @var string
     */
    const VERSION = '1.0.0';

    /**
     * 默认配置
     * @var array
     */
    protected array $config = [
        // 接口身份相关
        'appId'          => '',    // 插件ID
        'accessKey'      => '',    // 开发者秘钥(请自行向小破站申请)
        'secretKey'      => '',    // 开发者秘钥(请自行向小破站申请)
        // 请求设置
        'httpTimeout'    => 30.00, // HTTP请求超时
        // 日志设置
        'logPath'        => '',    // 留空则不开启日志
        'logRecycleDay'  => 30,    // 日志保留天数
        'logRecycleRate' => 10,    // 日志回收概率 设置 1-100% 设置为0则从不回收
    ];

    /**
     * 构造函数
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = array_merge($this->config, $config);
    }

    /**
     * 获取配置信息
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * 魔术方法: 获取操作类
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        // 当前需求服务的类名
        $serviceName = ucfirst(strtolower($name));
        $classNamespace = "\\eValor\\BiliOpenLiveSdk\\Service\\" . $serviceName;

        // 服务不存在则异常
        if (!class_exists($classNamespace)) {
            throw new Exception("The service {$serviceName} does not exist.");
        }

        // 实例化类并返回
        return new $classNamespace($this->config);
    }
}
