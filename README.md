# BiliOpenLiveSdk for PHP

> 需求版本 PHP7.4+

便于快速接入互动玩法相关接口

## 使用样例

请自行参考下方的使用案例

```PHP

<?php

use eValor\BiliOpenLiveSdk\BiLiveSdk;

require_once 'vendor/autoload.php';

// 初始化直播Sdk
$sdk = new BiLiveSdk([
    'appId'          => 0000000000000, // 插件ID 请在开发者后台获取
    'accessKey'      => '000000000000000000000000', // 开发者秘钥(请查看开发者申请时对应的邮箱)
    'secretKey'      => '0000000000000000000000000000000', // 开发者秘钥(请查看开发者申请时对应的邮箱)
    'httpTimeout'    => 30.00, // HTTP请求超时(保持默认或不设置)
    'logPath'        => dirname(__FILE__) . '/BiLiveSdk.log', // 留空则不开启日志
    'logRecycleDay'  => 30,    // 日志保留天数
    'logRecycleRate' => 10,    // 日志回收概率 设置 1-100% 设置为0则从不回收
]);

// 获取直播API服务
$liveSkd = $sdk->live;
$liveSkd->appStart('0000000000000'); // 使用主播身份码启动一个场次的游戏
$liveSkd->appHeartbeat('00000000-0000-0000-0000-000000000000'); // 发送后端心跳
$liveSkd->appBatchHeartbeat('00000000-0000-0000-0000-000000000000'); // 批量发送后端心跳
$liveSkd->appEnd('00000000-0000-0000-0000-000000000000'); // 结束一个场次游戏

```
